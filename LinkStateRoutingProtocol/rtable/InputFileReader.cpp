#include "InputFileReader.h"

#include <cstring>

InputFileReader::InputFileReader(IOBase* io)
{
	__io = io;
}


InputFileReader::~InputFileReader()
{
	// delete[] __path;
}

int* InputFileReader::Loadfile(char* path, int option) {
	bool all_neg_infty = (option & InputFileReader::ALL_NEGATIVE_INFINITY) == InputFileReader::ALL_NEGATIVE_INFINITY,
		self_auto_zero = (option & InputFileReader::SELF_AUTO_ZERO) == InputFileReader::SELF_AUTO_ZERO,
		fill_infty = (option & InputFileReader::FILL_INFINITY) == InputFileReader::FILL_INFINITY,
		ignore_zero = (option & InputFileReader::IGNORE_ZERO) == InputFileReader::IGNORE_ZERO;

	__eof = false;
	__path = path;

	int bufferDim = 4;
	int* buffer = new int[bufferDim * bufferDim];

	// Open input file
	if (!(__fp = fopen(path, "r"))) {
		sprintf_s(__io->Buffer, __io->BufferSize, "Can't open file %s", path);
		__io->Print(__io->Buffer);
		return nullptr;
	}

	// read objects
	int numCount = 0,			// count of total numbers
		lineNum = 0,			// count of line numbers, can't > firstLineCount
		lineCount = 0,			// count of current line numbers, can't > firstLineCount
		firstLineCount = 0,		// count of first line numbers
		input = 0;				// current number
	char c;
	bool isNum = false, isNeg = false, eof = false;

	while (fscanf(__fp, "%c", &c) != EOF) {
		if (c == '-' && !isNeg) {
			isNeg = true;
		}
		else if (c == '-' && isNeg) {
			sprintf_s(__io->Buffer, __io->BufferSize,
				"Error input value at line %d, %dth number!", lineNum + 1, lineCount + 1);
			__io->PrintBuffer();
			return nullptr;
		}

		while (c >= '0' && c <= '9') {
			input = input * 10 + c - '0';
			isNum = true;
			if (EOF == fscanf(__fp, "%c", &c)) { c = '\n'; break; }
		}

		// found non-numeric numbers (usually seperators)
		if ((' ' == c || '\n' == c || ',' == c || ';' == c || '\t' == c) && isNum) {
			if (firstLineCount > 0 && lineCount >= firstLineCount) {
				// If there are more number in some line...
				if (fill_infty) {
					sprintf_s(__io->Buffer, __io->BufferSize, "Found unexpected numbers at line %d, and had ignored.", lineNum + 1);
					__io->Print(__io->Buffer);
				}
				else {
					sprintf_s(__io->Buffer, __io->BufferSize, "Found unexpected numbers at line %d, program terminates.", lineNum + 1);
					__io->Print(__io->Buffer);
					return nullptr;
				}
			} else if (isNum) {
				// If already have numbers

				// check if it is SELF
				if (lineCount == lineNum && input != 0) {
					if (self_auto_zero) { 
						sprintf_s(__io->Buffer, __io->BufferSize,
							"Non-zero value at line %d, %dth number! Use 0 instead.", lineNum + 1, lineNum + 1);
						__io->PrintBuffer();
						input = 0; isNeg = false;
					}
					else {
						sprintf_s(__io->Buffer, __io->BufferSize,
							"Non-zero value at line %d, %dth number! Terminating...", lineNum + 1, lineNum + 1);
						__io->PrintBuffer();
						return nullptr;
					}
				}
				// check if there is any SELF at non-SELF area
				else if (lineCount != lineNum && input == 0) {
					if (ignore_zero) {
						sprintf_s(__io->Buffer, __io->BufferSize,
							"Detect zero at line %d, %dth number! Use -1 instead.", lineNum + 1, lineCount + 1);
						__io->PrintBuffer();
						input = 1; isNeg = true;
					}
					else {
						sprintf_s(__io->Buffer, __io->BufferSize,
							"Detect zero at line %d, %dth number! Terminating...", lineNum + 1, lineCount + 1);
						__io->PrintBuffer();
						return nullptr;
					}
				}

				// handle negative values
				if (lineCount != lineNum && isNeg) { 
					if (input != 1) {
						if (all_neg_infty) { 
							sprintf_s(__io->Buffer, __io->BufferSize,
								"Negative value in line %d, %dth is not -1! Use -1 instead.", lineNum + 1, lineCount + 1);
							__io->PrintBuffer();
							input = 1;
						}
						else {
							sprintf_s(__io->Buffer, __io->BufferSize,
								"Negative value in line %d, %dth is not -1! Terminating.", lineNum + 1, lineCount + 1);
							__io->PrintBuffer();							
							return nullptr;
						}
					}
					input = -input; 
				}
				buffer[numCount] = input;
				input = 0; numCount++; lineCount++;
				if (numCount >= bufferDim * bufferDim && bufferDim != firstLineCount) {	// Double the buffer size
					int* tmpBuf = new int[bufferDim * bufferDim * 4];
					memcpy(tmpBuf, buffer, bufferDim * sizeof(int));
					delete[] buffer; buffer = tmpBuf; bufferDim *= 2;
				}
				isNum = false; isNeg = false;
			}
		}

		if ('\n' == c) {
			if (0 == firstLineCount) {
				// If this is the first line ...
				firstLineCount = numCount;
				int* tmpBuf = new int[firstLineCount * firstLineCount];
				memcpy(tmpBuf, buffer, firstLineCount * sizeof(int));
				delete[] buffer; buffer = tmpBuf; bufferDim = __size = firstLineCount;
			} else if (lineCount < firstLineCount) {
				while (lineCount < firstLineCount) {
					buffer[numCount] = INFINITY;
					numCount++; lineCount++;
				}
			}

			lineNum++; lineCount = 0;
		}

		if (lineNum == firstLineCount && firstLineCount > 0) { break; }
	}

	sprintf_s(__io->Buffer, __io->BufferSize,
		"All done, in total %d lines", lineNum + 1);
	__io->PrintBuffer();

	__topology = buffer;

	return __topology;
}

int* InputFileReader::GetTopology() {
	return this->__topology;
}

void InputFileReader::OutputTopology() {
	if (nullptr == __topology) {
		__io->Print("Topology file is not loaded!\n");
		return;
	}

	for (int ii = 0; ii < __size; ii++) {
		for (int jj = 0; jj < __size; jj++) {
			sprintf_s(__io->Buffer, __io->BufferSize, "%d", __topology[ii * __size + jj]);
			__io->WriteBuffer();
			__io->Write(" ");
		}
		__io->Write("\n");
	}
}

int InputFileReader::Size() {
	return __size;
}