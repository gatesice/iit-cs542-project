#pragma once

#include "InputFileReader.h"
#include "RoutingProtocol.h"

class CLIMenu
{
public:
	CLIMenu();
	~CLIMenu();

	int MainMenu();

private:
	InputFileReader* __reader;
	RoutingProtocol* __routing;

	void __welcome();
	void __menu();
	void __loadfile();
	void __buildtable();
	void __buildalltables();
	void __showpath();
	void __exit();

	void __print_NewLine(const int& = 1);
	void __print_Spaces(const int&);

	int __input_Numerical();
	int __input_Numerical(const int&, const int&);
	int __input_YesNo();
	char* __input_String();

	int __t;

public:
	const int CONTINUE = 0x0000;
	const int TERMINATE = 0x0001;

	const int YES = 0x0001;
	const int NO = 0x0000;

	const int ENTER = 13;
};

