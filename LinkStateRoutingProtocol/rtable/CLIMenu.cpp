#include "CLIMenu.h"

#include <cstdio>

#include "IO.h"
#include <Windows.h>
#include <conio.h>

CLIMenu::CLIMenu()
{
	Console* io = new Console{ 3 };
	__reader = new InputFileReader{ io };
	__routing = new RoutingProtocol{};
	__routing->SetIO(io);
	__t = CONTINUE;
}


CLIMenu::~CLIMenu()
{
}

int CLIMenu::MainMenu() {
	__welcome();

	while (__t == CONTINUE) { 
		__menu(); 

		switch (__input_Numerical(1, 5)) {
		case 1:
			__loadfile();
			break;
		case 2:
			__buildtable();
			break;
		case 3:
			__buildalltables();
			break;
		case 4:
			__showpath();
			break;
		case 5:
			__exit();
			break;
		default:
			printf("You inputed error number!\n");
		}

		system("pause");
	}

	return 0;
}

// private methods

void CLIMenu::__welcome() {
	system("cls");

	__print_Spaces(8); printf("\nWelcome to Link State Routing Emulator!\n");

	system("pause");
}

void CLIMenu::__menu() {
	system("cls");

	printf("\nCS542 Link State Routing Simulator\n\n");
	printf("(1) Input Network Topology File\n");
	if (__routing->Size() > 0) {
		printf("(2) Build a Connection Table\n");
		printf("(3) Build all Connection Tables\n");
		printf("(4) Shortest Path to Destination Router\n");
	}
	printf("(5) Exit\n\n");
	printf("Command >");
}

void CLIMenu::__loadfile() {
	system("cls");

	printf("\nInput Network Topology File\n\n");
	printf("Input original network topology matrix data file:");
	char* filename = __input_String();

	int option = 0;
	printf("\nDo you want to treat all negative value as infinity? [y/n]");
	option |= (YES == __input_YesNo() ? InputFileReader::ALL_NEGATIVE_INFINITY : 0);
	printf("\nDo you want to ensure the same router has value 0? [y/n]");
	option |= (YES == __input_YesNo() ? InputFileReader::SELF_AUTO_ZERO : 0);
	printf("\nDo you want to set 0 in non-self area to -1? [y/n]");
	option |= (YES == __input_YesNo() ? InputFileReader::IGNORE_ZERO : 0);
	printf("\nDo you want to set lacking values to -1? [y/n]");
	option |= (YES == __input_YesNo() ? InputFileReader::FILL_INFINITY : 0);
	printf("\n\n");
	if (nullptr == __reader->Loadfile(filename, option)) {
		printf("\n    An error occured while loading topology file");
		return;
	}

	printf("\nReview original topology matrix:\n");
	__reader->OutputTopology();
	__routing->LoadTopology(__reader->GetTopology(), __reader->Size());
}

void CLIMenu::__buildtable() {
	if (__routing->Size() == 0) {
		return;
	}
	system("cls");


	printf("\nSleect a source router:");
	int targetTable = __input_Numerical(1, __routing->Size());
	__routing->RefreshTable(targetTable - 1);		// Index starts at 0
	__routing->OutputTable(targetTable - 1);			// Index starts at 0
}

void CLIMenu::__buildalltables() {
	if (__routing->Size() == 0) {
		return;
	}
	system("cls");

	__routing->RefreshAllTables();
	__routing->OutputAllTables();
}

void CLIMenu::__showpath() {
	if (__routing->Size() == 0) {
		return;
	}
	system("cls");

	printf("\nSelect a source router:");
	int src = __input_Numerical(1, __routing->Size());
	printf("\nSelect a destination router:");
	int dst = __input_Numerical(1, __routing->Size());
	if (src == dst) {
		printf("\nInputed the same router!");
		return;
	}
	__routing->OutputRoute(src, dst);
}

void CLIMenu::__exit() {
	__t = TERMINATE;
}

void CLIMenu::__print_NewLine(const int& lines) {
	for (int ii = 0; ii < lines; ii++) {
		printf("\n");
	}
}

void CLIMenu::__print_Spaces(const int& num) {
	for (int ii = 0; ii < num; ii++) {
		printf(" ");
	}
}

int CLIMenu::__input_Numerical() {
	int state = 0, value = 0;

	goto tag_input;
	while (!state) {
		printf("\nInput error! Please reinput :");
	tag_input:
		_flushall();
		state = scanf_s("%d", &value);
	}

	return value;
}

int CLIMenu::__input_Numerical(const int& min, const int& max) {
	int value = 0;

	goto tag_input_num;
	while (value < min || value > max) {
		printf("\n[%d-%d]?", min, max);
	tag_input_num:
		value = __input_Numerical();
	}

	return value;
}

int CLIMenu::__input_YesNo() {
	goto tag_yesno;
	while (true) {
		printf("[y/n]?");
	tag_yesno:
		char c = _getch();
		if ('y' == c || 'Y' == c) { return YES; }
		else if ('n' == c || 'N' == c) { return NO; }
	}
}

char* CLIMenu::__input_String() {
	char* str; str = new char[1024];
	_flushall();
	gets(str);
	return str;
}