#pragma once

#define TEST_INPUT_FILE_READER

#include "IO.h"

class InputFileReaderTest
{
public:
	InputFileReaderTest(IOBase*);
	~InputFileReaderTest();

	void LoadFileTest(char* path, int options = 0);

private:
	IOBase* __io;
};

