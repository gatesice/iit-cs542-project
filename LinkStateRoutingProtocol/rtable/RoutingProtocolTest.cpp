#include "RoutingProtocolTest.h"

#include "RoutingProtocol.h"

RoutingProtocolTest::RoutingProtocolTest(IOBase* io)
{
	__io = io;
}


RoutingProtocolTest::~RoutingProtocolTest()
{
}

void RoutingProtocolTest::GetCostTest() {
	__io->Print("-----------------------------------------");
	__io->Print("Test cases for RoutingProtocol::GetCost()");
	__io->Print("        contains 4 test cases");

	RoutingProtocol* __rp;
	int topology[] = {	SELF,	2,		5,		1,		INFINITY,
						2,		SELF,	8,		7,		9,
						5,		8,		SELF,	INFINITY, 4,
						1,		7,		INFINITY, SELF,	2,
						INFINITY, 9,	4,		2,		SELF };
	__rp = new RoutingProtocol{ topology, 5 };
	if (5 == __rp->__size) { __io->Print("This topology map has 5 routers"); }
	else { __io->Print("ERROR: Number of routers in this topology map is not 5!"); }

	if (__rp->GetCost(0, 3) == 1) { __io->Print("cost(0-3) Correct"); }
	if (__rp->GetCost(2, 2) == SELF) { __io->Print("cost(2-2) Correct"); }
	if (__rp->GetCost(4, 0) == INFINITY) { __io->Print("cost(4-1) Correct"); }
	if (__rp->GetCost(6, 3) == ERROR) { __io->Print("cost(6-2) Returns error correctly!"); }
	
	__io->Write("\n\n\n");
}

void RoutingProtocolTest::RefreshTableTest(int index) {
	__io->Print("-----------------------------------------");
	__io->Print("Test cases for RoutingProtocol::RefreshTable()");
	__io->Print("        ");

	RoutingProtocol* __rp;
	int topology[] = { SELF, 2, 5, 1, INFINITY,
		2, SELF, 8, 7, 9,
		5, 8, SELF, INFINITY, 4,
		1, 7, INFINITY, SELF, 2,
		INFINITY, 9, 4, 2, SELF };
	__rp = new RoutingProtocol{ topology, 5 };
	__rp->SetIO(this->__io);
	if (5 == __rp->__size) { __io->Print("This topology map has 5 routers"); }
	else { __io->Print("ERROR: Number of routers in this topology map is not 5!"); }

	__rp->RefreshTable(index);
	__rp->OutputTable(index);

	__io->Write("\n\n\n");

}

void RoutingProtocolTest::OutputRouteTest() {
	__io->Print("-----------------------------------------");
	__io->Print("Test cases for RoutingProtocol::OutputRoute()");
	__io->Print("        ");

	RoutingProtocol* __rp;
	int topology[] = { SELF, 2, 5, 1, INFINITY,
		2, SELF, 8, 7, 9,
		5, 8, SELF, INFINITY, 4,
		1, 7, INFINITY, SELF, 2,
		INFINITY, 9, 4, 2, SELF };
	__rp = new RoutingProtocol{ topology, 5 };
	__rp->SetIO(this->__io);
	if (5 == __rp->__size) { __io->Print("This topology map has 5 routers"); }
	else { __io->Print("ERROR: Number of routers in this topology map is not 5!"); }

	__rp->OutputRoute(1, 5);

	__io->Write("\n\n\n");
}