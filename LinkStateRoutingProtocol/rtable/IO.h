#pragma once

#include <cstdio>

class IOBase
{
public:
	IOBase(int level = 0);
	virtual ~IOBase();

	virtual void Print(char*, int level = 0) = 0;
	virtual void Write(char*) = 0;
	void PrintBuffer();
	void WriteBuffer();
	int Level;
	char* Buffer;
	int BufferSize;
	void InitializeBuffer(int bufferSize);
};

class Console : public IOBase {
public:
	Console(int level = 0);
	virtual ~Console();

	void Print(char*, int level = 0);
	void Write(char*);
};