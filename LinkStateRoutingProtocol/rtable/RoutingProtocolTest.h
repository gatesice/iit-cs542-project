#pragma once

#define TEST_ROUTING_PROTOCOL

#include "IO.h"

class RoutingProtocolTest
{
public:
	RoutingProtocolTest(IOBase*);
	~RoutingProtocolTest();

	void GetCostTest();
	void RefreshTableTest(int);
	void OutputRouteTest();

private:
	IOBase* __io;
};

