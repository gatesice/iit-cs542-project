#pragma once

#include "IO.h"
#include "RoutingProtocolTest.h"

const int SELF = 0;
const int INFINITY = -1;
const int ERROR = -2;
const int NONE = -3;

class RoutingProtocol
{
public:
	RoutingProtocol();
	RoutingProtocol(int*, const int&);
	virtual ~RoutingProtocol();

private:
	int* __topology;		// network topology
	int __size;				// network size

	int** __tables;			// routing tables

	IOBase* __io;				// Output handler

public:
	void LoadTopology(int*, const int&);	// Load an topology
	int GetCost(int, int);		// Get distance between two routers.

	void RefreshTable(int);			// Refresh specific routing table
	void RefreshAllTables();		// Refresh all routing tables

	int GetRouting(int, int, bool autoGen);
									// Get an specific routing record.
	void OutputTable(int, IOBase* io = nullptr);
									// Output an specific routing table
	void OutputAllTables(IOBase* io = nullptr);
									// Output all routing tables
	void OutputRoute(const int&, const int&, IOBase* io = nullptr);
	void SetIO(IOBase*);			// Set an Output driver
	int Size();

private:
	void __SetRouting(int, int, int targetInterface);	
									// Set an specific entry for a specific routing table
	int __MinCost(const int&, const int&);		// Return the minimal value between two values.
	void __Smaller(const int&, const int&, bool&, int&);

public:
#ifdef TEST_ROUTING_PROTOCOL
	friend class RoutingProtocolTest;			// Let test driver access private members
#endif
};

