#include "InputFileReaderTest.h"

#include "InputFileReader.h"

InputFileReaderTest::InputFileReaderTest(IOBase* io)
{
	__io = io;
}


InputFileReaderTest::~InputFileReaderTest()
{
}

void InputFileReaderTest::LoadFileTest(char* path, int options) {
	__io->Print("--------------------------------------------");
	__io->Print("Test case for InputFileReaderTest::LoadFile");

	__io->Write("\nThe input file is: ");
	__io->Print(path);
	
	InputFileReader reader{ __io };
	if (nullptr == reader.Loadfile(path, options)) {
		__io->Print("The input file is not valid and the operation has terminated.");
	} else {
		__io->Print("\nOutput topology map:");

		for (int ii = 0; ii < reader.__size; ii++) {
			sprintf_s(__io->Buffer, __io->BufferSize,
				"LINE %d:", ii);
			__io->WriteBuffer();

			for (int jj = 0; jj < reader.__size; jj++) {
				sprintf_s(__io->Buffer, __io->BufferSize,
					" %d", reader.__topology[ii * reader.__size + jj]);
				__io->WriteBuffer();
			}

			__io->Write("\n");
		}
	}

	__io->Write("\n\n\n");
}