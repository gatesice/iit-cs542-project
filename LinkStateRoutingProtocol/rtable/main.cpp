#include "RoutingProtocol.h"
#include "InputFileReader.h"
#include "IO.h"
#include "CLIMenu.h"

#include <Windows.h>

#include "RoutingProtocolTest.h"
#include "InputFileReaderTest.h"

void menu() {
	CLIMenu menu{};
	menu.MainMenu();
}

void test() {

	Console console{ 3 };
	RoutingProtocolTest test{ &console };

	test.GetCostTest();
	test.RefreshTableTest(0);
	test.RefreshTableTest(1);
	test.RefreshTableTest(2);
	test.RefreshTableTest(3);
	test.RefreshTableTest(4);
	test.OutputRouteTest();

	InputFileReaderTest test_input{ &console };

	test_input.LoadFileTest("topology.txt");
	test_input.LoadFileTest("neg.txt");
	test_input.LoadFileTest("neg.txt", InputFileReader::ALL_NEGATIVE_INFINITY);
	test_input.LoadFileTest("self.txt");
	test_input.LoadFileTest("self.txt", InputFileReader::SELF_AUTO_ZERO);
	test_input.LoadFileTest("fill.txt");
	test_input.LoadFileTest("fill.txt", InputFileReader::FILL_INFINITY);
	test_input.LoadFileTest("zero.txt");
	test_input.LoadFileTest("zero.txt", InputFileReader::IGNORE_ZERO);

	system("pause");

}

int main(int argc, char** argv) {

	for (int i = 0; i < argc; i++) {
		if (0 == strcmp(argv[i], "-test")) { goto test; }
	}

	menu();
	return 0;

test:
	test();

	return 0;
}