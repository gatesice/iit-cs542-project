#include "RoutingProtocol.h"

#include <cstdlib>

RoutingProtocol::RoutingProtocol() {
	__io = nullptr;
	__topology = nullptr;
	__size = 0;
	__tables = nullptr;
}

RoutingProtocol::RoutingProtocol(int* topology, const int& size)
{
	__io = nullptr;
	__topology = nullptr;
	__size = 0;
	__tables = nullptr;

	LoadTopology(topology, size);
}


RoutingProtocol::~RoutingProtocol() {
	delete[] __topology;
	delete[] __tables;
}

void RoutingProtocol::LoadTopology(int* topology, const int& size) {
	if (nullptr != __topology) {
		delete[] __topology;
		delete[] __tables;
	}
	__topology = topology;
	__size = size;
	__tables = new int*[__size];
	for (int ii = 0; ii < __size; ii++) {
		__tables[ii] = nullptr;
	}
}

// src and dest starts at 0
// Remember to convert while call GetCost()
int RoutingProtocol::GetCost(int src, int dest) {
	int index = src * __size + dest;
	if (0 > index || src >= __size || dest >= __size || src < 0 || dest < 0) { return ERROR; }
	if (nullptr == __topology) { 
		if (__io) { __io->Print("Network topology in RoutingProtocol is not initialized!", 3); }
		return ERROR;
	}
	return __topology[index];
}

// index starts at 0
// Remember to convert while call RefreshTable()
void RoutingProtocol::RefreshTable(int index) {
	//if (__io) { 
	//	sprintf_s(__io->Buffer, __io->BufferSize, "Refreshing routing table with index %d", index);
	//	__io->Print(__io->Buffer);
	//}

	// Dijkstra's Algorithm
	int* M; M = new int[__size * 2];		// [index * 2]    : prev
											// [index * 2 + 1]: min cost
	bool* N; N = new bool[__size];			// All nodes in N represents this algorithm ends.
	int NC = __size - 1;					// Count of remaining nodes.
	for (int ii = 0; ii < __size; ii++) { M[ii * 2] = M[ii * 2 + 1] = NONE; N[ii] = false; }
	M[index * 2] = M[index * 2 + 1] = NONE; N[index] = true;

	// Initialization of Dijkstra's
	for (int jj = 0; jj < __size; jj++) {
		if (index == jj) { continue; }
		M[jj * 2] = index;
		M[jj * 2 + 1] = GetCost(index, jj);
	}

	// Finding Shortest Path
	while (NC > 0) {
		int minNode = NONE, minCost = NONE;
		for (int ii = 0; ii < __size; ii++) {
			if (N[ii]) { continue; }
			if (M[ii * 2 + 1] == INFINITY ? false : (minCost == NONE ? true : minCost > M[ii * 2 + 1])) {
				minNode = ii;
				minCost = M[ii * 2 + 1];
			}
		}
		N[minNode] = true; NC--;

		for (int jj = 0; jj < __size; jj++) {
			if (N[jj] || minNode == jj) { continue; }
			bool smaller = false;
			int smallerValue = NONE;
			__Smaller(M[jj * 2 + 1], GetCost(minNode, jj) == INFINITY ? INFINITY : (M[minNode * 2 + 1] + GetCost(minNode, jj)), 
				smaller, smallerValue);

			if (!smaller) {
				M[jj * 2] = minNode;
				M[jj * 2 + 1] = smallerValue;
			}
		}
	}

	// Write routing table for `index`
	if (nullptr != __tables[index]) { delete[] __tables[index]; }
	__tables[index] = new int[__size * 2];

	for (int ii = 0; ii < __size; ii++) {
		if (NONE == M[ii * 2]) {
			__tables[index][ii] = SELF;
			continue;
		}

		int back = ii;
		while (M[back * 2] != index) {
			back = M[back * 2];
		}

		__tables[index][ii] = back + 1;
	}
}

void RoutingProtocol::RefreshAllTables() {
	for (int ii = 0; ii < __size; ii++) {
		RefreshTable(ii);
	}
}

int RoutingProtocol::GetRouting(int src, int dest, bool autoGen = false) {
	if (nullptr == __tables) {
		if (__io) { __io->Print("Routing table list in RoutingProtocol is not initialized!", 3); }
		return ERROR;
	}

	if (nullptr == __tables[src - 1]) {
		if (!autoGen) {
			sprintf_s(__io->Buffer, __io->BufferSize, "Target routing table %d does not initialized!", src);
			__io->Print(__io->Buffer);
		}
		if (autoGen) { RefreshTable(src - 1); }
		else { return NONE; }
	}

	return __tables[src - 1][dest - 1];
}

// `index` starts from 0
void RoutingProtocol::OutputTable(int index, IOBase* io) {
	if (nullptr == io) { io = __io;	}
	if (nullptr == io) { return; }

	if (nullptr == __tables[index]) { 
		io->Print("Specific table is not available. (it's nullptr)");
		return; 
	}

	sprintf_s(io->Buffer, io->BufferSize, "Router %d Connection Table", index + 1);
	io->Print(io->Buffer);
	sprintf_s(io->Buffer, io->BufferSize, "%20s  %20s", "Destination", "Source");
	io->Print(io->Buffer);
	io->Print("==========================================");
	
	for (int ii = 0; ii < __size; ii++) {
		char *s2; s2 = new char[16];
		sprintf_s(s2, 16, "%d", __tables[index][ii]);
		sprintf_s(io->Buffer, io->BufferSize, "%20d  %20s", ii + 1,
			ii == index ? "-" : s2);
		io->Print(io->Buffer);
	}
}

void RoutingProtocol::OutputAllTables(IOBase* io) {
	for (int ii = 0; ii < __size; ii++) {
		OutputTable(ii, io);
	}
}

void RoutingProtocol::OutputRoute(const int& src, const int& dst, IOBase* io) {
	if (nullptr == io) { io = __io; }
	if (nullptr == io) { return; }

	sprintf_s(io->Buffer, io->BufferSize,
		"The shortest path from router %d to router %d is ", src, dst);
	io->WriteBuffer();

	int forward = src, last = -1, cost = 0;
	for (int ii = 1; ii < __size && forward != dst; ii++) {
		sprintf_s(io->Buffer, io->BufferSize, "%d", forward); io->WriteBuffer();
		io->Write("-");
		last = forward;
		forward = GetRouting(forward, dst, true);
		cost += GetCost(last - 1, forward - 1);
	}
	sprintf_s(io->Buffer, io->BufferSize, "%d", forward); io->WriteBuffer();
	sprintf_s(io->Buffer, io->BufferSize, ", the total cost is %d", cost); io->PrintBuffer();
}


void RoutingProtocol::SetIO(IOBase* io) {
	__io = io;
}

int RoutingProtocol::Size() {
	return __size;
}

//
// private functions for RoutingProtocol
//

void RoutingProtocol::__SetRouting(int src, int dest, int targetInterface) {
	if (nullptr == __tables[src]) {
		if (__io) { __io->Print("Target routing table does not initialized. Please call RefreshTable() first!", 2); }
		return;
	}
	__tables[src][dest] = targetInterface;
	return;
}

int RoutingProtocol::__MinCost(const int& a, const int& b) {
	if (a < -1 || b < -1) { return ERROR; }

	if (INFINITY == b) { return a; }
	if (INFINITY != a && b > a) { return a; }
	return b;
}

void RoutingProtocol::__Smaller(const int& a, const int& b, bool& smaller, int& smallerCost) {
	if (a < -1 || b < -1) { smaller = false; }

	if (INFINITY == a) {
		smaller = false;
		smallerCost = b;
		return;
	}
	if (a > b && b != INFINITY) { smaller = false; smallerCost = b; return; }

	smaller = true; smallerCost = a;
}