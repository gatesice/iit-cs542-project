#pragma once
#include <cstdio>

#include "IO.h"
#include "RoutingProtocol.h"

#include "InputFileReaderTest.h"

class InputFileReader
{
public:
	InputFileReader(IOBase*);
	~InputFileReader();

	int* Loadfile(char* path, int option = 0);
	int* GetTopology();
	void OutputTopology();
	int Size();


private:
	FILE* __fp;
	int* __topology;
	char* __path;
	IOBase* __io;
	bool __eof;
	int __size;

public:
	static const int NORMAL = 0x00;
	static const int ALL_NEGATIVE_INFINITY = 0x01;	// When meets negative value, treat it as infinity
	static const int SELF_AUTO_ZERO = 0x02;			// All elements in row = col treated as SELF
	static const int FILL_INFINITY = 0x04;			// If some lines has lesser elements, treated as infinity.
	static const int IGNORE_ZERO = 0x08;			// If ZERO when row != col, treated as infinity.

#ifdef TEST_INPUT_FILE_READER
	friend class InputFileReaderTest;
#endif
};

