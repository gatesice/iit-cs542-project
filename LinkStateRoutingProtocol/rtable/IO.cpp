#include "IO.h"


IOBase::IOBase(int level)
{
	Level = level;
	InitializeBuffer(1024);
}


IOBase::~IOBase()
{
}

void IOBase::InitializeBuffer(int bufferSize) {
	delete[] Buffer;
	BufferSize = bufferSize;
	Buffer = new char[BufferSize];
}

void IOBase::PrintBuffer() {
	Print(Buffer);
}

void IOBase::WriteBuffer() {
	Write(Buffer);
}

Console::Console(int level) : IOBase(0) {

}

Console::~Console() {

}

void Console::Print(char* msg, int level) {
	if (level > this->Level) { return; }
	switch (level) {
	case 1:
		printf("[INFO] ");
	case 2:
		printf("[WARNING] ");
	case 3:
		printf("[ERROR] ");
	case -1:
		printf("[DEBUG] ");
	case 0: default: break;
	}

	printf("%s\n", msg);

	return;
}


void Console::Write(char* msg) {
	printf("%s", msg);

	return;
}